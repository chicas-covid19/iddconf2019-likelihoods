\documentclass[xcolor=dvipsnames]{beamer}

\usepackage{graphicx}
\usepackage{pgf}
\usepackage{multicol}
\usepackage{array}
\usepackage{bm}
\usepackage{tikz}
\usetikzlibrary{positioning,calc,fit,arrows,backgrounds}
\usepackage{float}
\usepackage{blkarray}
\usepackage{amsmath,amssymb}
\usepackage[english]{babel}
\pgfdeclareimage[height=0.5cm]{clogo}{clogo}
\logo{\pgfuseimage{clogo}}

\definecolor{LancsRed}{RGB}{139,14,29}
\definecolor{LancsGrey}{RGB}{176,179,181}
\definecolor{LightGray}{gray}{0.8}

\mode<presentation>{
	\usetheme{Madrid}
	\setbeamercovered{invisible}
	\usecolortheme{beaver}
}

\AtBeginSection[]
{
	\begin{frame}<beamer>
		\frametitle{Outline}
		\tableofcontents[currentsection,currentsubsection]
	\end{frame}
}

\title[Epidemic likelihoods]{Introduction to Likelihood-based Inference for Epidemic Models}
\author{Chris Jewell}
\institute{Lancaster University}
\date{IDDConf 2019 \\
      \url{http://fhm-chicas-code.lancs.ac.uk/jewell/iddconf2019-likelihoods}}

\begin{document}

\begin{frame}
\maketitle
\end{frame}

\begin{frame}
\frametitle{Aims}

We will:
\begin{enumerate}
\item Review likelihood theory (briefly!)
\item Connect epidemic simulation and inference
\item Dispel some common misconceptions (mathematical vs statistical models)
\item Construct an approach for generating likelihood functions for any stochastic epidemic model
\end{enumerate}
\vspace{12pt}
We will not:
\begin{enumerate}
\item Cover use of Bayesian statistics
\item Address missing/censored data issues
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Outline}
\tableofcontents
\end{frame}

\section{Random variables}

\begin{frame}
\frametitle{What is a random variable?}

\begin{itemize}
\item A variable whose \textcolor{blue}{observed} values depend on an underlying \textcolor{blue}{random} phenomenon
\end{itemize}
\begin{figure}[H]
\centering
\includegraphics[height=5cm]{normal-curve.pdf} \includegraphics[height=5cm]{binomial-pmf.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Some notation}

\begin{itemize}
\item \textcolor{blue}{Data generating model}
\vspace{12pt}
\item \textcolor{blue}{Simulation} of realisations of RV
\end{itemize}

\begin{block}{}
\begin{eqnarray*}
W & \sim & \mbox{Normal}(\mu, \sigma^2) \\
Z & \sim & \mbox{Binomial}(n, p) \\
\end{eqnarray*}
\end{block}
\end{frame}

\begin{frame}
\frametitle{Some notation}

\begin{itemize}
\item \textcolor{blue}{Probability density function}
  \begin{itemize}
  \item Continuous random variable
  \item e.g. $Pr(x_1 < X < x_2 | \mu, \sigma)$
  \end{itemize}

\begin{block}{}
$$f(w | \mu, \sigma) = \frac{1}{\sqrt{2\pi}\sigma} e^{-\frac{1}{2\sigma^2}\left(x - \mu\right)^2}$$
\end{block}
\vspace{12pt}
\item \textcolor{blue}{Probability mass function}
  \begin{itemize}
  \item Discrete random variable
  \item e.g. $Pr(X = x | n, p)$
  \end{itemize}
\begin{block}{}
$$f(z | n, p) =  {n \choose z} p^z (1-p)^{n-z}$$
\end{block}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Example}
\framesubtitle{Failure times}
A computer components manufacturer is interested in measuring mean time to failure...

\begin{itemize}
\item Observe time to failure of $n=100$ components: $y_1,\dots,y_n$
\item Time to failure: assume \textcolor{blue}{exponential} distribution with rate $\lambda$
\end{itemize}
\vspace{-24pt}
\begin{figure}[H]
\centering
\includegraphics[height=6cm]{failure-time-hist.pdf}
\end{figure}

\begin{itemize}
\item What is $\lambda$?
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Example}
\framesubtitle{Likelihood function}

\begin{itemize}
\item Each failure is an \textcolor{blue}{independent} event
\item \textcolor{blue}{Joint} probability density function \textcolor{blue}{product} of PDFs
$$f(y_1,\dots,y_n | \lambda) = \prod_{i=1}^{n} \lambda e^{-\lambda y_i}$$
\vspace{24pt}
\item The \textcolor{red}{likelihood} function is viewed as a function of $\lambda$
$$L(\lambda; y_1,\dots,y_n) \propto \prod_{i=1}^{n} \lambda e^{-\lambda y_i}$$
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Likelihood inference}
\framesubtitle{Log likelihood}
$$L(\lambda; y_1,\dots,y_n) \propto \prod_{i=1}^{n} \lambda e^{-\lambda y_i}$$

\begin{itemize}
\item Product over PDFs $\rightarrow 0$
\item Work with \textcolor{blue}{log} likelihood function
$$\ell(\lambda; y_1,\dots,y_n) \propto n \log \lambda - \lambda\sum_{i=1}^{n} y_i$$
\end{itemize}

\vspace{-36pt}
\begin{figure}[H]
\centering
\includegraphics[height=5cm]{failure-time-log-lik.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Likelihood inference}
\framesubtitle{Maximum likelihood inference -- MLE}

\textbf{Definition}: the value of $\lambda$ that maximises the probability of observing $\bm{y}$ under the model
$$\hat{\lambda} = \underset{\lambda}{\mbox{argmax}} \,\ell(\lambda; \bm{y})$$
\vspace{-36pt}
\begin{figure}[H]
\centering
\includegraphics[height=5cm]{failure-time-mle.pdf}
\end{figure}
\vspace{-24pt}
\begin{itemize}
\item Curvature gives us the \textcolor{blue}{standard error} (Fisher Information)
\item Maximisation by hand or \textcolor{blue}{numerical optimisation}
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Maximum likelihood estimation exercise}

\begin{itemize}
	\item A study of a non-communicable disease starts at $t=0$ and observes the following times of disease onset:
	$$\bm{y} = \{116.7, 69.4, 20.0, 60.0, 5.1\}$$
	\item Assume $y \overset{iid}{\sim} Exponential(\lambda)$
\end{itemize}
\begin{enumerate}
	\item Write down a log likelihood function $\ell(\lambda; \bm{y})$
	\item Plot the likelihood function for values of $\lambda \in [0.0001, 0.4]$
	\begin{itemize}
		\item Can you eyeball the MLE?
	\end{itemize}
	\item Optimise the log likelihood function to find the MLE numerically
	\item Calculate the Standard Error of $\hat{\lambda}$
\end{enumerate}

\end{frame}


\section[Simulation]{Epidemic data generating process}

\begin{frame}
\frametitle{Data generating process}
\framesubtitle{A general approach}
\begin{itemize}
	\item Epidemic models $\subset$ State Transition Models
	\item States, Transitions
\end{itemize}

\begin{figure}[h]
	\centering
	\begin{tikzpicture}[
	auto,
	state/.style={
		font={\sffamily\bfseries},
		align=center,
		anchor=center,
		minimum size=35pt,
		node distance=60pt
	}
	]
	\node[draw,state] (S) {$\mathcal{S}$};
	\node[draw,state,right=of S] (V) {$\mathcal{V}$};
	\node[draw,state,below=of V] (I) {$\mathcal{I}$};
	\node[draw,state,right=of I] (R) {$\mathcal{R}$};
	\draw [->] (S) to node (SV) {$\lambda^{SV}(t)$} (V);
	\draw [->] (S) to node[swap] (SI) {$\lambda^{SI}(t)$} (I);
	\draw [->] (V) to node (VI) {$\lambda^{VI}(t)$} (I);
	\draw [->] (I) to node[swap] (IR) {$\lambda^{IR}(t)$} (R);
	\draw [-(,dashed] (I) to [out=180,in=270] (SI);
	\draw [-(,dashed] (I) to [out=0,in=0] (VI);
	\end{tikzpicture}
\end{figure}

\end{frame}


\begin{frame}
\frametitle{Data generating process}
\framesubtitle{A simplified example -- Homogeneous SIR model}

\begin{figure}[h]
	\centering
	\begin{tikzpicture}[
	auto,
	state/.style={
		font={\sffamily\bfseries},
		align=center,
		anchor=center,
		minimum size=35pt,
		node distance=60pt
	}
	]
	\node[draw,state] (S) {$\mathcal{S}$};
	\node[draw,state,right=of S] (I) {$\mathcal{I}$};
	\node[draw,state,right=of I] (R) {$\mathcal{R}$};
	\draw [->] (S) to node (SI) {$\lambda^{SI}(t)$} (I);
	\draw [->] (I) to node (IR) {$\lambda^{IR}(t)$} (R);
	\draw [-(,dashed] (I) to [out=235,in=270] (SI);
	\end{tikzpicture}
\end{figure}

\begin{itemize}
\item Time inhomogeneous \textcolor{blue}{Poisson process}:
\begin{eqnarray*}
\lambda^{SI}(t) & = & \beta | \mathcal{I}(t) || \mathcal{S}(t) | \\
\lambda^{IR}(t) & = & \gamma | \mathcal{I}(t) |
\end{eqnarray*}
\vspace{12pt}
\item Rates are additive: $\Lambda(t) = \lambda^{SI}(t) + \lambda^{IR}(t)$
\end{itemize}

\end{frame}


\begin{frame}
\frametitle{Data generating process}
\framesubtitle{Continuous time}

Gillespie algorithm demonstrates link to \textcolor{blue}{Survival Analysis}

\begin{block}{Gillespie algorithm}
	\textbf{Input}: $t=0$, initial state $\{\mathcal{S}(0), \mathcal{I}(0), \mathcal{R}(0)\}$, parameters
	\begin{enumerate}
		\item Draw time to next event $t^\star \sim Exponential\left(\lambda^{SI}(t) + \lambda^{IR}(t)\right)$
		\item Choose \textbf{infection} w.p.
		$$\frac{\lambda^{SI}(t)}{\lambda^{SI}(t) + \lambda^{IR}(t)}$$
		\item $t = t + t^\star$, update state
		\item Goto 1 until $\lambda^{SI}(t) + \lambda^{IR}(t) = 0$
	\end{enumerate}
\end{block}
\end{frame}


\begin{frame}
\frametitle{Data generating process}
\framesubtitle{Markov property}
\begin{figure}
	\centering
	\includegraphics[height=3cm]{sir_timeline.pdf}
\end{figure}

\begin{itemize}
	\item State of the epidemic $Y(t) = \{\mathcal{S}(t), \mathcal{I}(t), \mathcal{R}(t)\}$
	\vspace{12pt}
	\item \textcolor{blue}{Markov} property: $Y(t_k) | Y(t_{k-1})$
	\begin{itemize}
		\item State at time $t_2$ depends only on state at time $t_1$
	\end{itemize} 
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Data generating process}
\framesubtitle{Survival analysis}

In survival analysis with (constant) \textcolor{blue}{hazard} $\lambda$
\begin{itemize}
	\item Survivor function: $Pr(T>t|\lambda) = S(t | \lambda) = 1 - F(t|\lambda)$
	\item Event distribution: $f(t|\lambda) = \lambda S(t | \lambda)$
\end{itemize}
\vspace{24pt}
Exponential distribution:
\begin{eqnarray*}
	S(t | \lambda) & = & e^{-\lambda t} \\
	f(t | \lambda) & = & \lambda e^{-\lambda t}
\end{eqnarray*}
\end{frame}



\begin{frame}
\frametitle{Data generating process}
\framesubtitle{Towards a likelihood}
\begin{columns}
	\begin{column}{0.5\textwidth}
		\begin{figure}
			\centering
			\includegraphics[height=3cm]{sir_timeline.pdf}
		\end{figure}
	\end{column}
	\begin{column}{0.5\textwidth}
		\begin{eqnarray*}
			\lambda^{SI}(t) & = & \beta | \mathcal{I}(t) ||\mathcal{S}(t) | \\
			\lambda^{IR}(t) & = & \gamma | \mathcal{I}(t) |
		\end{eqnarray*}
	\end{column}
\end{columns}


\begin{eqnarray*}
	Pr(t_1, \mbox{infection} | Y(t_0)) & = & (\textcolor{blue}{2\beta + \gamma}) e^{-(2\beta + \gamma)(t_1 - t_0) } \cdot \frac{\textcolor{red}{2\beta}}{\textcolor{blue}{2\beta + \gamma}} \\
	Pr(t_2, \mbox{removal} | Y(t_1)) & = & (\textcolor{blue}{2\beta + 2\gamma}) e^{-(2\beta + 2\gamma)(t_2 - t_1) } \cdot \frac{\textcolor{red}{2\gamma}}{\textcolor{blue}{2\beta + 2\gamma}} \\
	Pr(t_3, \mbox{removal} | Y(t_2)) & = & \textcolor{blue}{\gamma} e^{-\gamma(t_3 - t_2)} \cdot \frac{\textcolor{red}{\gamma}}{\textcolor{blue}{\gamma}}
\end{eqnarray*}
\end{frame}


\begin{frame}
\frametitle{Data generating process}
\framesubtitle{Towards a likelihood}
\begin{columns}
	\begin{column}{0.5\textwidth}
		\begin{figure}
			\centering
			\includegraphics[height=3cm]{sir_timeline.pdf}
		\end{figure}
	\end{column}
	\begin{column}{0.5\textwidth}
		\begin{eqnarray*}
			\lambda^{SI}(t) & = & \beta | \mathcal{I}(t) ||\mathcal{S}(t) | \\
			\lambda^{IR}(t) & = & \gamma | \mathcal{I}(t) |
		\end{eqnarray*}
	\end{column}
\end{columns}

\vspace{-12pt}
\begin{eqnarray*}
	Pr(t_1, \mbox{infection} | Y(t_0)) & = & \textcolor{red}{2\beta} e^{-(2\beta + \gamma)(t_1 - t_0) } \\
	Pr(t_2, \mbox{removal} | Y(t_1)) & = & \textcolor{red}{2\gamma} e^{-(2\beta + 2\gamma)(t_2 - t_1) } \\
	Pr(t_3, \mbox{removal} | Y(t_2)) & = & \textcolor{red}{\gamma} e^{-(\beta + \gamma)(t_3 - t_2) }
\end{eqnarray*}
\end{frame}



\begin{frame}
\frametitle{Data generating process}
\framesubtitle{Towards a likelihood}
\begin{columns}
	\begin{column}{0.5\textwidth}
		\begin{figure}
			\centering
			\includegraphics[height=3cm]{sir_timeline.pdf}
		\end{figure}
	\end{column}
	\begin{column}{0.5\textwidth}
		\begin{eqnarray*}
			\lambda^{SI}(t) & = & \beta | \mathcal{I}(t) ||\mathcal{S}(t) | \\
			\lambda^{IR}(t) & = & \gamma | \mathcal{I}(t) | \\
			\Lambda(t) & = & \lambda^{SI}(t) + \lambda^{IR}(t)
		\end{eqnarray*}
	\end{column}
\end{columns}

\begin{eqnarray*}
	Pr(t_1, \mbox{infection} | Y(t_0)) & = & \lambda^{SI}(t_0) e^{-\Lambda(t_0)(t_1 - t_0) } \\
	Pr(t_2, \mbox{removal} | Y(t_1)) & = & \lambda^{IR}(t_1) e^{-\Lambda(t_1)(t_2 - t_1) } \\
	Pr(t_3, \mbox{removal} | Y(t_2)) & = & \lambda^{IR}(t_2) e^{-\Lambda(t_2)(t_3 - t_2) }
\end{eqnarray*}
\end{frame}

\begin{frame}
\frametitle{Data generating process}
\framesubtitle{Survival analysis in a Markov setting}

\begin{itemize}
	\item \textcolor{blue}{Markov} model implies \textcolor{blue}{conditional independence}
	\begin{block}{Joint probability density function}
	\begin{eqnarray*}
		f(Y(t_{1,2,3}) | Y(t_0), \beta, \gamma) & = & \prod_{k=1}^{3} f(Y(t_k) | Y(t_{k-1}, \beta, \gamma)) \\
		& = & \prod_{k=1}^3 \lambda^k(t_{k-1}) S(t_k - t_{k-1} | Y(t_{k-1})) 
	\end{eqnarray*}
$\lambda^k(t)$ \textcolor{blue}{hazard rate} for the transition occurring at time $t_k$.
    \end{block}
  \item N.B. Condition on \textcolor{red}{initial conditions} $Y(t_0)$.
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Exercise 1}

In a population of size 5, the following event times were observed:
\begin{table}[H]
	\centering
	\begin{tabular}{ll}
		\hline
		\textbf{Infections} & 0.24, 1.74 \\
		\textbf{Removals} & 2.83, 8.57, 11.33 \\
		\hline
	\end{tabular}
\end{table}

\begin{enumerate}
\item Assuming an homogeneous Markov SIR model with density-dependent transmission, calculate
	$$f(\bm{Y} | Y_{t_0} = \{4, 1, 0\}, \beta=0.01, \gamma=0.14)$$
\item Using your favourite optimiser, calculate the MLEs for $\hat{\beta}$ and $\hat{\gamma}$ for the given event history.
\begin{itemize}
	\item Hint: you may need to replace $\beta = e^{\beta^\star}$ and $\gamma = e^{\gamma^\star}$.  Why?
\end{itemize}
\end{enumerate}

\end{frame}


\begin{frame}
\frametitle{Exercise 2}

\begin{itemize}
	\item Suppose we have a \textcolor{purple}{vaccinated} class
\end{itemize}
\begin{figure}[h]
	\centering
	\begin{tikzpicture}[
	auto,
	scale=0.5,
	state/.style={
		font={\sffamily\bfseries},
		align=center,
		anchor=center,
		minimum size=25pt,
		node distance=40pt
	}
	]
	\node[draw,state] (S) {$\mathcal{S}$};
	\node[draw,state,right=of S] (V) {$\mathcal{V}$};
	\node[draw,state,below=of V] (I) {$\mathcal{I}$};
	\node[draw,state,right=of I] (R) {$\mathcal{R}$};
	\draw [->] (S) to node (SV) {$\lambda^{SV}(t)$} (V);
	\draw [->] (S) to node[swap] (SI) {$\lambda^{SI}(t)$} (I);
	\draw [->] (V) to node (VI) {$\lambda^{VI}(t)$} (I);
	\draw [->] (I) to node[swap] (IR) {$\lambda^{IR}(t)$} (R);
	\draw [-(,dashed] (I) to [out=180,in=270] (SI);
	\draw [-(,dashed] (I) to [out=0,in=0] (VI);
	\end{tikzpicture}
\end{figure}

\begin{enumerate}
	\item What is a good data generating model?
	\item Write down (but don't necessarily implement) a suitable likelihood function for this model.
\end{enumerate}

\end{frame}


\begin{frame}
\frametitle{Heterogeneous Epidemics}

\begin{itemize}
	\item So far we have covered \textcolor{blue}{homogeneously-mixing} models
	\begin{itemize}
		\item Individuals all \textcolor{blue}{identical}
		\item Events are \textcolor{blue}{unlabelled}
	\end{itemize}
	\item Likelihood complexity $\mathcal{O}(K)$ for $K$ events.
	\vspace{12pt}
	\item How does this extend to \textcolor{blue}{heterogeneous} populations?
	\begin{itemize}
		\item Contact networks
		\item Individual-level covariates
	\end{itemize}
\end{itemize}

\end{frame}



\begin{frame}
\frametitle{Adding population heterogeneity}
\framesubtitle{Contact networks}

\begin{itemize}
\item In reality, individuals are connected by \textcolor{blue}{network}s.
\item<1-> Consider a population of 5 people, \textcolor{red}{binary contact network}
\item<1-> Represent connectivity between individuals $i$ (rows) and $j$ (cols):
$$
C = \left( \begin{array}{ccc}
0 & 1 & 0 \\
1 & 0 & 1 \\
0 & 1 & 0 \end{array} \right)
$$
\vspace{12pt}
\item<2-> \textbf{Assumption}: $i$ can infect $j$ \emph{if and only if} $c_{ij}=1$
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Heterogeneous force of infection}
\framesubtitle{Network model}

\begin{columns}
	\begin{column}{2in}
		\begin{equation*}
		C = 
		\begin{blockarray}{cccc}
		\textcolor{Red}{a} & \textcolor{ForestGreen}{b} & \textcolor{ForestGreen}{c} \\
		\begin{block}{(ccc)c}
		0 & \textcolor{blue}{1} & 0 & \textcolor{Red}{a} \\
		1 & 0 & 1 & \textcolor{ForestGreen}{b}\\
		0 & 1 & 0 & \textcolor{ForestGreen}{c}\\
		\end{block}
		\end{blockarray}
		\end{equation*}
	\end{column}
	
	\begin{column}{2in}
		\begin{figure}
			\centering
			\begin{tikzpicture}[
			auto,
			node distance=24pt,
			circ/.style={
				circle,
				draw=black,
				align=center,
				minimum height=24pt,
				minimum width=24pt}]
			\node[circ, fill=ForestGreen] (B) {$B$};
			\node[circ, fill=Red, above right=of S] (A) {$A$};
			\node[circ,fill=ForestGreen, above left=of S] (C) {$C$};
			\draw[-,very thick] (A) to node (I1S) {$\beta$} (B);
			\draw[-,very thick] (B) to node (I2S) {$\beta$} (C);
			\end{tikzpicture}
		\end{figure}
	\end{column}
\end{columns}
\vspace{12pt}
\begin{itemize}
	\item $i$ infects $j$ \emph{if and only if} $c_{ij}=1$ \textcolor{red}{and} $i$ is infected
	\vspace{12pt}
	\item<2-> At time $t$, \textcolor{blue}{force of infection} on individual \textcolor{ForestGreen}{$b$}:
	$$
	\lambda_{b}(t) = \beta c_{ab}
	$$
\end{itemize}

\end{frame}



\begin{frame}
\frametitle{Individual-level model}
\framesubtitle{Data generating process}

\begin{itemize}
	\item $\textcolor{ForestGreen}{\mathcal{S}}\rightarrow\textcolor{red}{\mathcal{I}(t)}$: At $t$, each susceptible individual $j$ experiences
	$$\lambda_j^{SI}(t) = \beta \sum_{i \in \mathcal{I}} c_{ij},\;\;j\in \mathcal{S}(t)$$
	\item $\textcolor{red}{\mathcal{I}}\rightarrow\textcolor{blue}{\mathcal{R}}$: constant removal rate
	$$\lambda_j^{IR}(t) = \gamma,\;\;j\in\mathcal{I}(t)$$
\end{itemize}

\end{frame}



\begin{frame}
\frametitle{Individual-level model}
\framesubtitle{Data generating process}

\begin{block}{Gillespie algorithm}
	\textbf{Input}: initial state $Y(t_0)$, $t=0$, $\beta$, $\gamma$
	\begin{enumerate}
		\item $t^\star \sim \mbox{Exponential}(\sum_{j \in \mathcal{S}(t)} \lambda_j(t) + \gamma |\mathcal{I}(t)|)$
		\item Choose event $$k \sim \mbox{Discrete} \left( \left\{\{\lambda_j^{SI}(t)\}: j\in\mathcal{S}(t), \{\lambda_j^{IR}(t)\}: j\in\mathcal{I}(t)\right\} \right)$$
		\item Update $t=t+t^\star$, $Y(t)$
		\item Goto 1 until $$\sum_{j\in{\mathcal{S}(t)}} \lambda^{SI}(t) + \sum_{j\in\mathcal{I}(t)} \lambda^{IR}(t) = 0$$
	\end{enumerate}
\end{block}

\end{frame}



\begin{frame}
\frametitle{Individual-level model likelihood function}

Each event $k$ identified by time \textcolor{blue}{$t$}, type \textcolor{blue}{$r$}, individual \textcolor{blue}{$j$}

\begin{columns}
	\begin{column}{0.5\textwidth}
		\begin{figure}
			\centering
			\includegraphics[height=3cm]{sir_timeline.pdf}
		\end{figure}
	\end{column}
	\begin{column}{0.5\textwidth}
		\begin{eqnarray*}
			\lambda_j^{SI}(t) & = & \beta \sum_{i\in\mathcal{I}(t)} c_{ij} \\
			\lambda_j^{IR}(t) & = & \gamma \\
			\Lambda(t) & = & \sum_{j\in\mathcal{S}(t)} \lambda_j^{SI}(t) + \sum_{j\in\mathcal{I}(t)} \gamma
		\end{eqnarray*}
	\end{column}
\end{columns}

\begin{eqnarray*}
	Pr(t_1, \mbox{infection} | Y(t_0)) & = & \textcolor{red}{\lambda_B^{SI}(t_0)} e^{-\Lambda(t_0)(t_1 - t_0) } \\
	Pr(t_2, \mbox{removal} | Y(t_1)) & = & \textcolor{red}{\lambda_A^{IR}(t_1)} e^{-\Lambda(t_1)(t_2 - t_1) } \\
	Pr(t_3, \mbox{removal} | Y(t_2)) & = & \textcolor{red}{\lambda_B^{IR}(t_2)} e^{-\Lambda(t_2)(t_3 - t_2) }
\end{eqnarray*}

\end{frame}



\begin{frame}
\frametitle{Individual-level model likelihood function}

\begin{eqnarray*}
f(\bm{Y} | Y(t_0), \beta, \gamma) & = & \prod_{k} \lambda_{j_k}^{r_k}(t_{k-1}) e^{-\Lambda(t_{k-1})(t_{k} - t_{k-1})} \\
& = & \prod_{k: r_k=SI} \lambda_j^{SI}(t_{k-1}) \cdot \prod_{k:r_k=IR} \lambda_{j_k}^{IR}(t_{k-1}) \cdot e^{-\int_0^T \Lambda(t)dt}
\end{eqnarray*}
over time interval $[0, T=\max t)$.

\begin{equation*}
\log f(\bm{Y} | Y(t_0), \beta, \gamma) =  \sum_{k} \log \lambda_{j_k}^{r_k}(t_{k-1}) - \int_0^T \Lambda(t_{k-1})(t_{k} - t_{k-1})
\end{equation*}

\end{frame}



\begin{frame}
\frametitle{Complexity}
\begin{columns}
	\begin{column}{0.5\textwidth}
		\begin{figure}
			\centering
			\includegraphics[height=3cm]{sir_timeline.pdf}
		\end{figure}
	\end{column}
	\begin{column}{0.5\textwidth}
		\begin{eqnarray*}
			\lambda_j^{SI}(t) & = & \beta \sum_{i\in\mathcal{I}(t)} c_{ij} \\
			\lambda_j^{IR}(t) & = & \gamma \\
			\Lambda(t) & = & \sum_{j\in\mathcal{S}(t)} \lambda_j^{SI}(t) + \sum_{j\in\mathcal{I}(t)} \gamma
		\end{eqnarray*}
	\end{column}
\end{columns}
\vspace{12pt}
\begin{itemize}
	\item ILM complexity $\mathcal{O}(2K^2 N)$, $K$ number of events, $N$ population size
	\item Slowest computation is \textcolor{blue}{integrated transition rate}:
	$$ E = \int_0^T \Lambda(t_{k-1})(t_{k} - t_{k-1})$$
	\item Na\"{i}ve approach calculates $\Lambda(t)$ for each event timestep.
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Individual-level model likelihood function}
\framesubtitle{Complexity reductions}

\begin{figure}[h]
	\centering
	\begin{tikzpicture}[
	auto,
	state/.style={
		font={\sffamily\bfseries},
		align=center,
		anchor=center,
		minimum size=35pt,
		node distance=60pt
	}
	]
	\node[draw,state] (S) {$\mathcal{S}$};
	\node[draw,state,right=of S] (I) {$\mathcal{I}$};
	\node[draw,state,right=of I] (R) {$\mathcal{R}$};
	\draw [->] (S) to node (SI) {$\lambda^{SI}(t)$} (I);
	\draw [->] (I) to node (IR) {$\lambda^{IR}(t)$} (R);
	\draw [-(,dashed] (I) to [out=235,in=270] (SI);
	\end{tikzpicture}
\end{figure}

\begin{itemize}
	\item In ILM, events are \textcolor{blue}{labelled} by individual, e.g. $t^{SI}_j\, t^{IR}_j$
	\vspace{12pt}
	\item Use \textcolor{blue}{Survivor} function for an individual's \textcolor{blue}{sojourn} in a given state:
	\begin{eqnarray*}
		S(t_2 - t_1 | Y(t_1), j) = e^{-\int_{t_1}^{t_2} \lambda_j(t) dt}
	\end{eqnarray*}
where $\int_{t_1}^{t_2} \lambda_j(t) dt$ is \textcolor{blue}{integrated transition rate} out of the state.
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Simplifying the integral}

\begin{itemize}
	\item If transition rate constant, e.g. $\lambda_j(t) = \gamma$
	$$e^{-\int_{t_1}^{t_2} \lambda_j(t) dt} = e^{-\gamma (t_2 - t_1)}$$
	\vspace{12pt}
	\item For $\lambda^{SI}(t)$ we can \textcolor{blue}{discretise} the integral...
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Discretising the integral}
\framesubtitle{Infection process}

\begin{figure}
	\centering
	\includegraphics[height=3cm]{sir_timeline2.pdf}
\end{figure}

\begin{itemize}
	\item B exposed to A during $[t_0, t_1)$ 
	\item C exposed to A during $[t_0, t_2)$
	\item C exposed to B during $[t_1, t_3)$
	\vspace{12pt}
	\item Intersection between two intervals:
	$$[s_0, s_1) \cup [t_0, t_1) = (s_1 \wedge t_1 - s_0 \vee t_0) \vee 0$$
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Discretising the integral}
\framesubtitle{Infection process}

\begin{itemize}
	\item Since all individuals (apart from initial) start in $\mathcal{S}$
	$$E = \sum_{j \in P} \sum_{i \in \mathcal{I}(\cdot)} \beta c_{ij} \left[ R_i \wedge I_j - I_j \wedge I_i \right]$$
	\vspace{12pt}
	\item Complexity $\mathcal{O}(n_I N) < \mathcal{O}(2K^2N)$
	\vspace{12pt}
	\item Proof left to the reader!
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Putting it all together}
\framesubtitle{Markov individual-based SIR model}

\begin{block}{Model}
	\small
	\textbf{States}: S, I, R
	\begin{eqnarray*}
		\lambda_j^{SI}(t) & = & \beta \sum_{i \in \mathcal{I}(t)} c_{ij} \\
		\lambda_j^{IR}(t) & = & \gamma
	\end{eqnarray*}
\end{block}

\begin{block}{Likelihood function}
	\small
\begin{eqnarray*}
	f(\bm{Y} | Y(t_0), \beta, \gamma) & = & \prod_{j: t_0 < t^{SI}_j < T} \lambda^{SI}_j(t) \gamma \\
	& \times & \exp \left[ -\sum_{j \in P} \sum_{i \in \mathcal{I}(\cdot)} \beta c_{ij} \left[ R_i \wedge I_j - I_j \wedge I_i \right] + \sum_{j \in \mathcal{I}(\cdot)} \gamma (R_j - I_j) \right] \\
	\end{eqnarray*}
\end{block}
\end{frame}


%\begin{frame}
%\frametitle{Exercise}
%
%A population of 5 individuals has a contact matrix
%$$
%C = \left( \begin{array}{ccccc}
%0 & 1 & 0 & 1 & 1\\
%1 & 0 & 1 & 0 & 1\\
%0 & 1 & 0 & 1 & 0\\
%1 & 0 & 1 & 0 & 0 \\
%1 & 1 & 0 & 0 & 0\end{array} \right)
%$$
%
%
%
%
%\end{frame}


\begin{frame}
\frametitle{Concluding remarks}

\begin{itemize}
	\item \textcolor{blue}{Recipe} for constructing a likelihood function for an epidemic
	\begin{itemize}
		\item Homogeneous vs. heterogeneous models
		\item How to use the likelihood to make inference \textcolor{blue}{given full data}
	\end{itemize}
	\vspace{12pt}
	\item Simulation and inference are \textcolor{blue}{inextricable}
	\begin{itemize}
		\item Models are a probabilistic construct
	\end{itemize}
	\vspace{12pt}
	\item Likelihood functions are \textcolor{blue}{hard}
	\begin{itemize}
		\item Keep a clear view of the \textcolor{blue}{data generating process}
	\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Further reading}

\begin{itemize}
	\item Bayesian implementations and \textcolor{blue}{data augmentation} for censored events
	\begin{itemize}
		\item O'Neil (2002) Mathematical Biosciences \textbf{180}:103--114
		\item Jewell \emph{et al.} (2009) Prev. Vet. Med. \textbf{91}:19--28
		\item Jewell \emph{et al.} (2009) Bayesian Analysis. \textbf{4}:465--496
	\end{itemize}
	\vspace{12pt}
	\item GPU acceleration of likelihood computations
	\begin{itemize}
		\item Probert \emph{et al.} (2018) PLoS Comp. Biol. \textbf{14}:e1006202
	\end{itemize}
	\vspace{12pt}
	\item Likelihood methods
	\begin{itemize}
		\item Severini. Likelihood methods in Statistics.  Oxford Scientific Series, 2001.
	\end{itemize}
	\item Bayesian analysis
	\begin{itemize}
		\item Gilks \emph{et al.} Markov Chain Monte Carlo in Practice. Chapman and Hall, 1996.
	\end{itemize}
\end{itemize}
\end{frame}


\end{document}